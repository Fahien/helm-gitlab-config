#!/usr/bin/env python3
#
# Copyright © 2020 Daniel Stone
# Copyright © 2020 Benjamin Tissoires
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# Authors: Daniel Stone <daniel@fooishbar.org>
#          Benjamin Tissoires <benjamin.tissoires@gmail.com>
#
#
# This script monitors for free space on the /var/lib/docker mount, and
# sweeps up older images to free space where available.

import click
import datetime
import git
import os
import parse
import pprint
import re
import yaml

import docker

from pathlib import Path


def kib(bytes):
    return bytes / 1024


def mib(bytes):
    return kib(bytes) / 1024


def gib(bytes):
    return mib(bytes) / 1024


def bytes_to_human(bytes):
    for fun, suffix in [(gib, 'GB'),
                        (mib, 'MB'),
                        (kib, 'KB'),
                        ]:
        v = fun(float(bytes))
        if v > 1:
            return f'{v:.2f}{suffix}'

    return str(bytes)


SLEEP_INTERVAL = 300
excludes = []


def docker_fromisoformat(date):
    if date.endswith('Z'):
        date = date.rstrip('Z')

    # enforce ms precision
    if '.' in date:
        # if the time in microseconds is not represented by 6 numbers,
        # datetime complains.
        date = date.ljust(26, '0')

        # strip out nanoseconds
        date = date[:26]

    date += '+00:00'

    return datetime.datetime.fromisoformat(date)


class FDOImage():
    def __init__(self, docker_server, docker_image):
        self.server = docker_server
        self.docker = docker_image
        self.id = self.docker.id
        self.tags = self.docker.tags
        self.is_running = False
        self.timestamp = datetime.datetime.fromtimestamp(0, datetime.timezone.utc)
        self.refcount = 0
        self.created = docker_fromisoformat(self.docker.attrs["Created"])
        self.labels = self.docker.labels
        self.size = self.docker.attrs["Size"]
        self.expiration_time = None
        self.is_upstream = False

        for t in self.tags:
            if t not in self.server.usages:
                continue
            usage = self.server.usages[t]
            self.refcount += usage['count']

            ts = docker_fromisoformat(usage['last_used_at'])
            if ts > self.timestamp:
                self.timestamp = ts

        self.__compute_labels()

    def __repr__(self):
        return "{!r}".format({"id": self.id, "tags": self.tags, "is_running": self.is_running, "timestamp": self.timestamp})

    def ref(self, timestamp, container=None):
        if container is not None and container.is_running:
            self.is_running = True
        return self

    def should_cleanup(self):
        # images that are running can't be pruned
        if self.is_running:
            return False

        # the image has been marked as expired, act accordingly
        if self.expired:
            return True

        # we don't clean up automatically upstream images
        if self.is_upstream:
            return False

        if self.is_protected_image:
            return False

        # the image has been used not so long ago
        if self.timestamp >= datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=1):
            return False

        # Protect some unknown images
        if self.refcount == 0 and self.tags:
            return False

        return True

    def __get_is_upstream(self):
        try:
            upstream = self.labels['fdo.upstream-repo']
        except KeyError:
            self.is_upstream = False
        else:
            self.is_upstream = bool([t for t in self.tags if t.startswith(f'registry.freedesktop.org/{upstream}')])

    def __get_expiration_time(self):
        try:
            expiration_time = self.labels['fdo.expires-after']
        except KeyError:
            return False
        else:
            r = parse.parse('{count:d}{unit}', expiration_time)
            if r is None:
                return False
            expiration = self.created
            unit = r['unit']
            count = r['count']
            if unit == 'h':
                expiration += datetime.timedelta(hours=count)
            elif unit == 'd':
                expiration += datetime.timedelta(days=count)
            elif unit == 'w':
                expiration += datetime.timedelta(days=count * 7)

            self.expiration_time = expiration

    def __compute_labels(self):
        self.__get_is_upstream()
        self.__get_expiration_time()

    @property
    def expired(self):
        if self.expiration_time is None:
            return False

        return datetime.datetime.now(datetime.timezone.utc) > self.expiration_time

    @property
    def is_protected_image(self):
        for exclusion in excludes:
            r = re.compile(exclusion)
            for t in self.tags:
                if r.match(t):
                    return True
        return False


class FDOVolume():
    def __init__(self, server, docker_volume):
        self.server = server
        self.docker = docker_volume
        self.id = self.docker.id
        self.is_running = False
        self.timestamp = datetime.datetime.fromtimestamp(0, datetime.timezone.utc)
        self.created_at = docker_fromisoformat(self.docker.attrs["CreatedAt"])
        self.refcount = 0

    def __repr__(self):
        return "{!r}".format({"id": self.id, "is_running": self.is_running, "timestamp": self.timestamp})

    @property
    def size(self):
        volume_path = Path('/var/lib/docker/volumes') / self.id
        return sum(f.stat().st_size for f in volume_path.glob('**/*') if f.is_file())

    def ref(self, container):
        if container is not None and container.is_running:
            self.is_running = True
        if container.timestamp > self.timestamp:
            self.timestamp = container.timestamp
        self.refcount += 1
        return self

    def unref(self):
        self.refcount -= 1

    # clean up volumes not referenced by any container; GitLab Runner uses
    # volumes as caches, but only finds them via container IDs, so we can
    # safely obliterate any unused ones
    def should_cleanup(self):
        return self.refcount == 0

    def remove(self):
        self.docker.remove()


class FDOContainer():
    def __init__(self, server, docker_container):
        self.server = server
        self.docker = docker_container
        self.id = self.docker.id
        self.is_running = (self.docker.status not in ["exited", "dead", "created"])
        self.timestamp = docker_fromisoformat(self.docker.attrs["Created"])
        self.finished_at = docker_fromisoformat(self.docker.attrs['State']['FinishedAt'])
        try:
            self.image = self.server.images[self.docker.image.id].ref(self.timestamp, self)
        except docker.errors.ImageNotFound:
            self.image = None
        self.volumes = map(lambda v: self.server.volumes[v['Name']].ref(self), filter(lambda m: m.get('Type', None) == 'volume', self.docker.attrs["Mounts"]))

    def __repr__(self):
        return "{!r}".format({"id": self.id, "image": self.image.id if self.image else None, "is_running": self.is_running, "time": self.timestamp})

    # clean up containers which exited more than an day ago
    def should_cleanup(self):
        if self.is_running:
            return False
        if self.timestamp >= datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=1):
            return False
        return True

    @property
    def size(self):
        return sum(v.size for v in self.volumes)

    def remove(self):
        for v in self.volumes:
            v.unref()
        return self.docker.remove(v=True)


class FDOServer():
    def __init__(self, verbose, usage_repo, **kwargs):
        self.docker = docker.from_env()
        self.usage_repo = usage_repo
        self.images = None
        self.volumes = None
        self.containers = None
        self.verbose = verbose
        self.usages = None

        self.refresh_docker()

    def refresh_docker(self):
        # fetch the latest usage stats
        update_image_stats(self.usage_repo)

        # build a usage map based on the data pulled
        self.usages = {}
        for path in self.usage_repo.iterdir():
            if path.name.startswith('images_'):
                with open(path) as f:
                    data = yaml.load(f, Loader=yaml.CLoader)
                for i in data:
                    usage = self.usages.get(i, data[i])
                    if i in self.usages:
                        usage['count'] += data[i]['count']
                    usage['last_used_at'] = data[i]['last_used_at']

                    self.usages[i] = usage

        self.images = {i.id: FDOImage(self, i) for i in self.docker.images.list()}
        self.volumes = {v.id: FDOVolume(self, v) for v in self.docker.volumes.list()}
        self.containers = {c.id: FDOContainer(self, c) for c in self.docker.containers.list(all=True, ignore_removed=True)}
        if self.verbose:
            print("Images:")
            for i in self.images.values():
                pprint.pprint(i, indent=4)
            print("Containers:")
            for c in self.containers.values():
                pprint.pprint(c, indent=4)

    def remove_image(self, victim, dry_run):
        if self.verbose or dry_run:
            print(f"Removing I {victim.tags} (ID: {victim.id}, ts: {victim.timestamp}, refcount:{victim.refcount})")
        if not dry_run:
            self.docker.images.remove(victim.id, force=True)
            return True
        return False

    def remove_container(self, victim, dry_run):
        if self.verbose or dry_run:
            print(f"Removing C {victim.id} created at {victim.timestamp})")
        if not dry_run:
            victim.remove()
            return True
        return False

    def remove_volume(self, victim, dry_run):
        if self.verbose or dry_run:
            print(f"Removing V (ID: {victim.id}, ts: {victim.timestamp}, created: {victim.created_at}, refcount:{victim.refcount})")
        if not dry_run:
            victim.remove()
            return True
        return False

    def clean_up_expired_images(self, dry_run):
        expired_images = filter(lambda i: not i.is_running and i.expired, self.images.values())
        reclaimed_space = 0
        for victim in list(expired_images):
            if self.remove_image(victim, dry_run):
                del(self.images[victim.id])
            reclaimed_space += victim.size

        if reclaimed_space > 0:
            print(f'reclaimed space: expired images - {bytes_to_human(reclaimed_space)}')

    def maybe_cleanup(self, dry_run, target_free_space):
        fs_size = get_disk_size()
        avail = get_available_space()
        print(f"{bytes_to_human(avail)} available from {bytes_to_human(fs_size)} total (target {target_free_space} GB)")

        if gib(avail) < target_free_space:

            clean_containers = filter(lambda c: c.should_cleanup(), self.containers.values())
            reclaimed_space = 0
            for victim in sorted(clean_containers, key=lambda c: c.finished_at):
                # we need to compute the size before, we are checking for the folder size
                reclaimed_space += victim.size

                if self.remove_container(victim, dry_run):
                    del(self.containers[victim.id])

                if gib(avail + reclaimed_space) > target_free_space:
                    break

            if reclaimed_space > 0:
                print(f'reclaimed space: containers - {bytes_to_human(reclaimed_space)}')

            clean_volumes = filter(lambda v: v.should_cleanup(), self.volumes.values())
            reclaimed_space = 0
            for victim in sorted(clean_volumes, key=lambda v: v.created_at):
                # we need to compute the size before, we are checking for the folder size
                reclaimed_space += victim.size

                if self.remove_volume(victim, dry_run):
                    del(self.volumes[victim.id])

                if gib(avail + reclaimed_space) > target_free_space:
                    break

            if reclaimed_space > 0:
                print(f'reclaimed space: volumes - {bytes_to_human(reclaimed_space)}')

            candidate_images = filter(lambda i: i.should_cleanup(), self.images.values())
            reclaimed_space = 0
            for victim in sorted(candidate_images, key=lambda i: (i.refcount, i.timestamp)):
                if self.remove_image(victim, dry_run):
                    del(self.images[victim.id])

                reclaimed_space += victim.size

                if gib(avail + reclaimed_space) > target_free_space:
                    break

            if reclaimed_space > 0:
                print(f'reclaimed space: images - {bytes_to_human(reclaimed_space)}')

    def process_start_event(self, e):
        image_id = e['from']
        if image_id in self.images:
            image = self.images[image_id]
            image.ref(datetime.datetime.fromtimestamp(e['time'], datetime.timezone.utc))

    def next_event(self, interval):
        started_at = datetime.datetime.now()
        for e in self.docker.events(decode=True, filters={'type': 'container'}):
            if self.verbose:
                print('.', end='')
            if datetime.datetime.now() - started_at > datetime.timedelta(seconds=interval):
                return True


def get_available_space():
    st = os.statvfs("/var/lib/docker")
    return st.f_bsize * st.f_bavail


def get_disk_size():
    st = os.statvfs("/var/lib/docker")
    return st.f_bsize * st.f_blocks


def get_default_target():
    fs_size = get_disk_size()
    if gib(fs_size) >= 500:
        return 150
    else:
        return 40


def update_image_stats(usage_repo, url=None):
    if not usage_repo.exists():
        git.Repo.clone_from(url, usage_repo)

    repo = git.Repo(usage_repo)

    origin = repo.remote('origin')
    origin.pull()


@click.command()
@click.option('--dry-run', default=False, is_flag=True, help='Do not actually do the cleanup, merely say what it would have done.')
@click.option('--verbose', default=False, is_flag=True, help='show a lot of infomration')
@click.option('--interval', default=SLEEP_INTERVAL, help='time to wait between 2 iterations. 0 means do one iteration only')
@click.option('--exclude-file',
              default='/etc/docker-gc-exclude',
              type=click.Path(dir_okay=False, readable=True),
              help='a file with a list of tags to exclude from the garbage collection. python regex are supported. (defaults to /etc/docker-gc-exclude)')
@click.option('--target',
              default=get_default_target(),
              type=int,
              help="The target of free spaces in GB we should have at the end of one cleaning run")
@click.option('--usage-repo-url',
              default='https://gitlab.freedesktop.org/freedesktop/fdo-containers-usage.git',
              help='the url of the repo with the images statistics')
@click.option('--usage-repo',
              default='fdo-containers-usage',
              type=click.Path(file_okay=False, dir_okay=True),
              help='the directory where we cloned the distant repo with the image usages')
def main(dry_run, verbose, interval, exclude_file, target, usage_repo, usage_repo_url):
    # read the exclusion tags
    exclude_file = Path(exclude_file)
    if exclude_file.exists():
        global excludes
        with open(exclude_file) as f:
            excludes = [x.strip() for x in f.readlines()]

    usage_repo = Path(usage_repo)
    update_image_stats(usage_repo, usage_repo_url)

    server = FDOServer(verbose, usage_repo)
    server.clean_up_expired_images(dry_run)
    server.maybe_cleanup(dry_run, target)

    if interval:
        while server.next_event(interval):
            server.refresh_docker()
            server.clean_up_expired_images(dry_run)
            server.maybe_cleanup(dry_run, target)


if __name__ == '__main__':
    main()
