#!/bin/sh

# you must set:
#  - $PACKET_PROJECT_ID to the fd.o CI Packet project
#  - $PACKET_DEVICE_NAME to the desired device name (e.g. fdo-packet-5)
#  - $GITLAB_RUNNER_REG_TOKEN to the shared-runner registration token from https://gitlab.freedesktop.org/admin/runners

packet device create --project-id $PACKET_PROJECT_ID --hostname $PACKET_DEVICE_NAME --plan m1.xlarge.x86 --facility ewr1 --operating-system debian_10 --userdata "$(./generate-cloud-init.py --instance-name $PACKET_DEVICE_NAME --gitlab-runner-registration-token $GITLAB_RUNNER_REG_TOKEN --gitlab-runner-concurrent 8)"
