package httpapi.authz

default allow = false

# keep owner rules, or we can not administer the server
allow = true {
  input.owner == true
}


fetch_only_operations := {
  "s3:HeadObject",
  "s3:GetObject",
}

read_only_operations := {
  "s3:GetObjectAcl",
  "s3:GetObjectLegalHold",
  "s3:GetObjectLockConfiguration",
  "s3:ListBucket",
  "s3:ListObjects",
  "s3:ListObjectsV2"
}

read_operations := fetch_only_operations | read_only_operations

write_only_operations := {
  "s3:DeleteObject",
  "s3:DeleteObjects",
  "s3:PutObject",
  "s3:PutObjectAcl",
  "s3:PutObjectLegalHold",
  "s3:PutObjectLockConfiguration"
}

write_operations := read_operations | write_only_operations

########################################################
# example of a JWT token claims
########################################################
#
#{
#  "input": {
#    "account": "<REDACTED>",
#    "action": "s3:ListBucket",
#    "bucket": "git-cache",
#    "conditions": <REDACTED>
#    "owner": false,
#    "object": "",
#    "claims": {
#      "accessKey": "<REDACTED>",
#      "exp": "1593596324",
#      "iat": 1593595394,
#      "iss": "gitlab.freedesktop.org",
#      "job_id": "3378898",
#      "jti": "<REDACTED>",
#      "namespace_id": "686",
#      "namespace_path": "bentiss",
#      "nbf": 1593595389,
#      "pipeline_id": "169859",
#      "project_id": "6576",
#      "project_path": "bentiss/test-minio",
#      "ref": "master",
#      "ref_protected": "false",
#      "ref_type": "branch",
#      "sub": "job_3378898",
#      "user_email": "benjamin.tissoires@gmail.com",
#      "user_id": "685",
#      "user_login": "bentiss"
#    }
#  }
#}


########################################################
# for non owners (JWT auth)
########################################################
fetch_repos := {
  "mesa-lava",
  "mesa-tracie-public",
  "mesa-tracie-results",
}

read_repos := {
  "git-cache",
  "artifacts",
  ""
}

# keep fetch for everyone on fetch_repos
allow = true {
  input.owner == false
  fetch_repos[input.bucket]
  fetch_only_operations[input.action]
}

# keep read for everyone on read_repos
allow = true {
  input.owner == false
  read_repos[input.bucket]
  read_operations[input.action]
}

# git cache

allow = true {
 input.owner == false
 input.bucket == "git-cache"

 # extract the project name from the project path
 project_name := trim_prefix(input.claims.project_path, sprintf("%v/", [input.claims.namespace_path]))
 input.object == concat("/", [input.claims.project_path, concat(".", [project_name, "tar.gz"])])
 write_operations[input.action]
}

# artifacts

allow = true {
 input.owner == false
 input.bucket == "artifacts"
 startswith(input.object, concat("/", [input.claims.project_path, input.claims.pipeline_id, ""]))
 write_operations[input.action]
}

# Mesa CI: LAVA jobs allow writing a rootfs/kernel/etc from jobs, then reading them back from anywhere
allow = true {
 input.owner == false
 input.bucket == "mesa-lava"
 startswith(input.object, concat("/", [input.claims.project_path, ""]))
 write_operations[input.action]
}

# Mesa CI: Tracie trace files can be written from traces-db jobs, and read from anywhere
allow = true {
 input.owner == false
 input.bucket == "mesa-tracie-public"
 input.claims.project_path == "gfx-ci/tracie/traces-db"
 write_operations[input.action]
}

# Mesa CI: Tracie can be written from mesa/mesa jobs, and read from anywhere
allow = true {
 input.owner == false
 input.bucket == "mesa-tracie-results"
 input.claims.project_path == "mesa/mesa"
 write_operations[input.action]
}
