#!/usr/bin/env python3
#
# Copyright © 2020 Daniel Stone
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# Authors: Daniel Stone <daniel@fooishbar.org>

import click
import os
import os.path
import requests
import yaml

# use literal rather than quoted encoding for block strings
# https://stackoverflow.com/a/33300001
def str_presenter(dumper, data):
  if len(data.splitlines()) > 1:  # check for multiline string
    return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')
  return dumper.represent_scalar('tag:yaml.org,2002:str', data)

yaml.add_representer(str, str_presenter)


class MyDumper(yaml.Dumper):
    def increase_indent(self, flow=False, indentless=False):
        return super(MyDumper, self).increase_indent(flow, False)

def get_local_file(filename):
    return open(os.path.join(os.path.dirname(__file__), filename)).read()

def get_remote_uri(uri):
    get = requests.get(uri)
    if not get.ok:
        get.raise_for_status()
    return get.text

cloud_init = {
    'package-update': True,
    'package-upgrade': True,
    'apt': {
        'sources': {
            'buster-backports.list': {
                'source': 'deb http://deb.debian.org/debian buster-backports main contrib non-free',
            },
        },
    },
    'packages': [
        # HTTPS archive verification
        'debian-archive-keyring',
        'apt-transport-https',
        # sensible updates
        'needrestart',
        # ??
        'patch',
        # network
        'wireguard',
        'ufw',
        # to install kgctl
        'git',
        'golang',
        # daniels
        'zsh',
        'vim',
    ],
    'runcmd': [
        # kernel-img.conf creates /boot/initrd.img when our grub expects /boot/initrd
        'ln -sf initrd.img /boot/initrd',

        # the upgrade by cloud-init doesn't upgrade the kernel
        'env DEBIAN_FRONTEND=noninteractive apt-get update',
        'env DEBIAN_FRONTEND=noninteractive apt-get -y dist-upgrade',

        # configure RAID for k3s storage
        'echo type=fd | sudo sfdisk /dev/sdc',
        'echo type=fd | sudo sfdisk /dev/sdd',
        'echo type=fd | sudo sfdisk /dev/sde',
        'echo type=fd | sudo sfdisk /dev/sdf',
        'mdadm --create --verbose /dev/md0 --level=5 --raid-devices=4 /dev/sdc1 /dev/sdd1 /dev/sde1 /dev/sdf1',
        'mkfs.ext4 -F /dev/md0',
        'echo \'UUID="\'$(blkid -s UUID -o value /dev/md0)\'" /var/lib/rancher ext4 defaults 0 0\' | tee -a /etc/fstab',
        'mkdir -p /var/lib/rancher',
        'mount -a',

        # switch to iptables-legacy, k3s doesn't support (yet) nftables
        'update-alternatives --set iptables /usr/sbin/iptables-legacy',
        'update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy',

        # configure ufw
        'ufw allow ssh',
        'ufw allow https',
        'ufw allow http',
        'ufw allow 51820/udp',  # wireguard
        'ufw default deny incoming',
        'ufw default allow outgoing',
        'ufw default allow routed',
        'systemctl enable --now ufw',
        'ufw enable',

        # download kilo sources for installing kgctl
        'git clone https://github.com/squat/kilo /root/kilo',
    ],
    'write_files': [
        {
            'path': '/etc/kernel-img.conf',
            'owner': 'root:root',
            'content': 'do_symlinks=Yes\nimage_dest=/boot',
        },
    ]
}


def add_k3s(instance_name):
    GITHUB_URL = 'https://github.com/kubernetes/dashboard/releases'
    VERSION_KUBE_DASHBOARD = requests.get(f'{GITHUB_URL}/latest').url.split('/')[-1]
    cloud_init['write_files'].extend([
        {
            'path': '/root/kilo-k3s.yaml',
            'owner': 'root:root',
            'permissions': '0644',
            'content': get_remote_uri('https://raw.githubusercontent.com/squat/kilo/master/manifests/kilo-k3s.yaml')
        },
        {
            'path': '/root/dashboard-rbac.yaml',
            'owner': 'root:root',
            'permissions': '0644',
            'content': get_local_file('dashboard-rbac.yaml')
        },
        {
            'path': '/root/dashboard.yaml',
            'owner': 'root:root',
            'permissions': '0644',
            'content': get_remote_uri(f'https://raw.githubusercontent.com/kubernetes/dashboard/{VERSION_KUBE_DASHBOARD}/aio/deploy/recommended.yaml')
        },
    ])
    cloud_init['runcmd'].extend([
        # k3s (https://rancher.com/docs/k3s/latest/en/quick-start/)
        'ufw allow from 127.0.0.1 proto tcp to any port 6443',  # k3s API
        'ufw allow from 10.0.0.0/8 proto tcp to any port 6443',
        'curl -sfL https://get.k3s.io | sh -s - server --no-flannel --no-deploy traefik',
        'ufw allow in on kilo0 from 10.0.0.0/8',
        'ufw allow in on kube-bridge from 10.0.0.0/8',
        'sleep 5',  # give a little bit of time for k3s to spin up
        f'kubectl wait --for=condition=Ready node/{instance_name}',
        f'kubectl annotate node {instance_name} kilo.squat.ai/location="{instance_name}"',

        # kilo (https://github.com/squat/kilo)
        'k3s kubectl apply -f /root/kilo-k3s.yaml',

        # dashboard
        'k3s kubectl apply -f /root/dashboard.yaml',
        'k3s kubectl apply -f /root/dashboard-rbac.yaml',
    ])

@click.command()
@click.option('--instance-name',
              type=click.STRING,
              required=True,
              help='Name for this instance')
def main(instance_name):
    add_k3s(instance_name)

    print('#cloud-config')
    # these options make the result more human readable
    print(yaml.dump(cloud_init, Dumper=MyDumper, indent=2, width=9999, default_flow_style=False))
    print("")

if __name__ == '__main__':
    main()
