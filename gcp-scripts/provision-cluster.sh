#!/bin/bash
#
# Copyright © 2018 Daniel Stone
# 
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# 
# Author: Daniel Stone <daniel@fooishbar.org>
#
#
# This script provisions a new test Kubernetes cluster and makes it available
# to deploy Helm to.
#
# The default values are used to stand up a test instance with the same
# configuration as gitlab.fd.o.

set -e
set -u
set -o pipefail

# Google Cloud Platform project to create the new cluster in
GCP_PROJECT="fdo-gitlab"

# Geographical zone to create GKE cluster and GCS buckets in
GCP_REGION="us-east1"
GCP_ZONE="us-east1-b"

# Cluster name, which will be used in e.g. kubectl
GKE_CLUSTER_NAME="fdo-gitlab-prod-n16"

# Google Kubernetes Engine addons to enable for this cluster:
#   https://cloud.google.com/kubernetes-engine/docs/reference/rest/v1/projects.locations.clusters#addonsconfig
GKE_ADDONS="HorizontalPodAutoscaling=ENABLED"

# Machine type to create the Kubernetes nodes on
GCE_NODE_TYPE="n1-standard-16"

# Minimum CPU generation to accept for Kubernetes nodes
GCE_NODE_CPU="Intel Skylake"

# Version of Kubernetes to run on the cluster.
K8S_VERSION="1.11.2-gke.15"

# Size of the root partitions: these will only contain the container images
# and the running rootfs themselves, with persistent data being stored on
# separate disks.
GCE_NODE_DISK_SIZE="100GB"
GCE_NODE_DISK_TYPE="pd-ssd"

# Prefix for storage bucket names
GCS_BUCKET_PREFIX="test-fdo-gitlab-test-"

# Name of IP address to create
GCP_IP_NAME="test-gitlab-test0"

# Sanity checks: try to make sure we don't end up deleting production stuff on a whim.
if [ -z "${FDO___ULTRA_SAFETY_OVERRIDE_NEVER_SET_THIS_IN_YOUR_ENVIRONMENT:-}" ]; then
	case "${GKE_CLUSTER_NAME}" in
		test-*)
			;;
		*)
			echo "GKE cluster name '${GKE_CLUSTER_NAME}' doesn't start with test-"
			echo ""
			echo ""
			echo "Will not continue without environment override."
			exit 1
			;;
	esac

	case "${GCS_BUCKET_PREFIX}" in
		test-*)
			;;
		*)
			echo "GCS bucket prefix '${GCS_BUCKET_PREFIX}' doesn't start with test-"
			echo ""
			echo ""
			echo "Will not continue without environment override."
			exit 1
			;;
	esac

	case "${GCP_IP_NAME}" in
		test-*)
			;;
		*)
			echo "GCP IP address name '${GCP_IP_NAME}' doesn't start with test-"
			echo ""
			echo ""
			echo "Will not continue without environment override."
			exit 1
			;;
	esac
else
	if [ -n "${FDO_DELETE_EXISTING_STUFF:-}" ]; then
		echo "Deleting existing cloud objects with name checks disabled is far too brave for me."
		echo "Manually remove this check from the script if automatically deleting potentially-production objects is really what you want."
		echo ""
		echo "(It probably isn't.)"
		exit 1
	fi
fi


if ! gcloud version >/dev/null 2>/dev/null; then
	echo "gcloud not installed. Please install it from:"
	echo "    https://cloud.google.com/sdk/install"
	exit 1
fi

if ! jq --help >/dev/null 2>/dev/null; then
	echo "jq not installed; please install it first."
	exit 1
fi

if ! helm home >/dev/null 2>/dev/null; then
	echo "Helm not installed; please install it from:"
	echo "    https://github.com/kubernetes/helm"
	exit 1
fi

echo "Checking access to GCP project ${GCP_PROJECT} ..."
if [ -z "$(gcloud projects list --format='value(projectId)' | egrep \^${GCP_PROJECT}\$)" ]; then
	echo "No access to ${GCP_PROJECT} project in GCP"
	echo ""
	echo "Currently authenticated GCP accounts:"
	gcloud auth list
	echo ""
	echo ""
	echo "Please log in to a sufficiently-credentialed GCP account using:"
	echo "    $ gcloud auth login"
	exit 1
fi
GCP_PROJECT_ID="$(gcloud projects describe fdo-gitlab --format='value(projectNumber)')"

echo "Checking if cluster ${GKE_CLUSTER_NAME} exists in ${GCP_ZONE} ... "
if gcloud --project="${GCP_PROJECT}" container clusters describe --format=none --zone="${GCP_ZONE}" "${GKE_CLUSTER_NAME}" 2>/dev/null; then
	echo "Kubernetes cluster '${GKE_CLUSTER_NAME}' already exists."
	echo ""

	if [ -n "${FDO_DELETE_EXISTING_STUFF:-}" ]; then
		echo "Deleting existing cluster ${GKE_CLUSTER_NAME} ..."
		gcloud --project="${GCP_PROJECT}" container clusters delete --zone="${GCP_ZONE}" "${GKE_CLUSTER_NAME}"
	else	
		echo "Currently active clusters:"
		gcloud --project="${GCP_PROJECT}" container clusters list --format='value(name)'
		echo ""
		echo ""
		echo "Refusing to continue without override."
		exit 1
	fi
fi

echo "Checking if IP address ${GCP_IP_NAME} exists in ${GCP_REGION} ..."
if gcloud --project="${GCP_PROJECT}" compute addresses describe --region "${GCP_REGION}" --format=none "${GCP_IP_NAME}" 2>/dev/null; then
	ip_status="$(gcloud --project="${GCP_PROJECT}" compute addresses describe --region "${GCP_REGION}" --format='value(status)' "${GCP_IP_NAME}")"
	if [ "$ip_status" = "IN_USE" ]; then
		echo "IP address ${GCP_IP_NAME} already exists in ${GCP_REGION} and is in use. You must release it, or choose another name, before continuing."
		echo ""
		echo ""
		gcloud --project="${GCP_PROJECT}" compute addresses describe --region "${GCP_REGION}" "${GCP_IP_NAME}"
		exit 1
	else
		echo "IP address ${GCP_IP_NAME} already exists in ${GCP_REGION}; reusing."
	fi
else
	echo "Creating IP address ${GCP_IP_NAME} in ${GCP_REGION} ..."
	gcloud --project="${GCP_PROJECT}" compute addresses create \
		--region="${GCP_REGION}" --network-tier=PREMIUM \
		"${GCP_IP_NAME}"
fi
GCP_IP_ADDRESS="$(gcloud --project="${GCP_PROJECT}" compute addresses describe --region="${GCP_REGION}" "${GCP_IP_NAME}")"

echo "Creating cluster ${GKE_CLUSTER_NAME} of ${GCE_NODE_TYPE} (${GCE_NODE_CPU}) in ${GCP_ZONE} running Kubernetes ${K8S_VERSION} ..."
gcloud --project="${GCP_PROJECT}" container clusters create \
	--zone="${GCP_ZONE}" \
	--machine-type="${GCE_NODE_TYPE}" --min-cpu-platform="${GCE_NODE_CPU}" \
	--disk-size="${GCE_NODE_DISK_SIZE}" --disk-type="${GCE_NODE_DISK_TYPE}" \
	--cluster-version="${K8S_VERSION}" \
	--enable-autorepair --enable-autoupgrade \
	--no-enable-basic-auth --no-issue-client-certificate \
	--no-enable-ip-alias \
	"${GKE_CLUSTER_NAME}"

echo "Getting authentication token for cluster ${GKE_CLUSTER_NAME} ..."
gcloud --project="${GCP_PROJECT}" container clusters get-credentials --zone="${GCP_ZONE}" "${GKE_CLUSTER_NAME}"

# XXX: Is this actually part of the gcloud get-credentials ABI, or will it change ... ?
#      I couldn't find a way to make it print the kubectl context name.
KUBE_CTX="gke_${GCP_PROJECT}_${GCP_ZONE}_${GKE_CLUSTER_NAME}"

# XXX: Tiller should probably have TLS enabled for authentication, and it should
#      also run under RBAC.
#      TLS isn't really an issue though, since we don't run untrusted apps on our
#      cluster or allow arbitrary access to network endpoints.
#      RBAC similarly isn't a huge issue: the cluster is dedicated purely to
#      running Helm charts, so we would just enumerate all the cluster resources
#      in RBAC configuration.
echo "Installing Helm on new cluster ..."
kubectl --context="${KUBE_CTX}" create serviceaccount --namespace kube-system tiller
kubectl --context="${KUBE_CTX}" create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --kube-context "${KUBE_CTX}" --wait --service-account tiller

# for i in artifacts data-backup lfs registry uploads; do
# 	bucket_name="${GCS_BUCKET_PREFIX}${i}"
# 	source_bucket_prod="fdo-gitlab-${i}"
# 
# 	if gsutil ls gs://${bucket_name}/ >/dev/null 2>/dev/null; then
# 		echo "Storage bucket ${bucket_name} already exists."
# 		if [ -n "${FDO_DELETE_EXISTING_STUFF:-}" ]; then
# 			echo "Deleting existing bucket ${bucket_name} ..."
# 			gsutil rm -r gs://${bucket_name}
# 		else
# 			echo "Not deleting existing bucket ${bucket_name}."
# 			echo ""
# 			echo ""
# 			echo "Currently existing buckets:"
# 			gsutil ls -p "${GCP_PROJECT_ID}"
# 			echo ""
# 			echo ""
# 			echo "Refusing to continue without override."
# 			exit 1
# 		fi
# 	fi
# 
# 	case "${i}" in
# 		data-backup)
# 			ret_args="--retention 7d"
# 			do_copy="false"
# 			;;
# 		*)
# 			ret_args=""
# 			do_copy="true"
# 			;;
# 	esac
# 
# 	echo "Creating bucket ${bucket_name} in ${GCP_REGION} ..."
# 	gsutil mb -p "${GCP_PROJECT_ID}" -l "${GCP_REGION}" ${ret_args} "gs://${bucket_name}"
# 
# 	echo "Copying IAM permissions from ${source_bucket_prod} to ${bucket_name} ..."
# 	gcs_iam_res_file="$(mktemp ${bucket_name}.XXXXXXXX)"
# 	gsutil iam get "gs://${source_bucket_prod}" > "${gcs_iam_res_file}"
# 	gsutil iam set -e '' "${gcs_iam_res_file}" "gs://${bucket_name}"
# 	rm "${gcs_iam_res_file}"
# 
# 	echo "Syncing files from ${source_bucket_prod} into ${bucket_name} ..."
# 	gsutil -m rsync -r -p "gs://${source_bucket_prod}" "gs://${bucket_name}" >/dev/null
# done

echo "Cluster is ready for use."
echo ""
echo "Helm is installed and may be used with:"
echo "    $ helm --kube-context=${KUBE_CTX} install [...]"
echo ""
echo "The ingress IP address is ${GCP_IP_ADDRESS}"
