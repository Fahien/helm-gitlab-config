#!/bin/bash

for disk in $(gcloud compute disks list --format='value(name)'); do
	echo checking $disk
	users=$(gcloud compute disks describe --zone=us-east1-b --format='value(users)' $disk | xargs echo -n)
	if [ -z "$users" ]; then
		echo "    ... disk not in use, last attached $(gcloud compute disks describe --zone=us-east1-b --format='value(lastAttachTimestamp)' $disk)"
		gcloud compute disks delete --zone=us-east1-b $disk
	else
		echo "    ... disk in use by $users"
	fi
done
