gitlab-omnibus:
  baseIP: 35.185.111.185

  mattermostEnable: false
  prometheusEnable: false

  gitlabDataStorageSize: 400Gi
  gitlabRegistryStorageSize: 2000Gi

  pagesExternalScheme: https
  pagesExternalDomain: pages.freedesktop.org
  pagesTlsSecret: gitlab-pages-tls

  # my kingdom for the ability to include files ... ?!
  gitPostReceiveMirrorHook: |
    #!/bin/bash

    export REMOTE_REPO="$(git config --local fdo.mirror-dir)"

    export GIT_SSH_COMMAND="ssh -o 'ClearAllForwardings yes' -o 'GlobalKnownHostsFile /dev/null' -o 'IdentitiesOnly yes' -o 'IdentityFile /gitlab-ssh-keys/kemper-ssh-priv' -o 'StrictHostKeyChecking yes' -o 'HashKnownHosts no' -o 'UserKnownHostsFile /gitlab-ssh-keys/kemper-ssh-known-hosts' -F /dev/null"

    while read oldrev newrev refname; do
      case "$refname" in
        refs/heads/*)
          git push ssh://gitlab-mirror@kemper.freedesktop.org/git/${REMOTE_REPO} +${newrev}:${refname} >/dev/null
          ;;
        refs/tags/*)
          git push ssh://gitlab-mirror@kemper.freedesktop.org/git/${REMOTE_REPO} +${refname} >/dev/null
          ;;
        *)
          ;;
      esac
    done

  omniauth:
    providers: |
      [
        {
          'name' => 'google_oauth2',
          'app_id' => {{ .Values.global.omnibus.oauthGoogleId | squote }},
          'app_secret' => ENV['OAUTH_GOOGLE_SECRET'],
          'args' => { 'access_type' => 'offline', 'approval_prompt' => '' }
        },
        {
          'name' => 'gitlab',
          'app_id' => {{ .Values.global.omnibus.oauthGitLabId | squote }},
          'app_secret' => ENV['OAUTH_GITLAB_SECRET'],
          'args' => { 'scope' => 'read_user' }
        },
        {
          'name' => 'github',
          'app_id' => {{ .Values.global.omnibus.oauthGitHubId | squote }},
          'app_secret' => ENV['OAUTH_GITHUB_SECRET'],
          'args' => { 'scope' => 'user:email' }
        },
        {
          'name' => 'twitter',
          'app_id' => {{ .Values.global.omnibus.oauthTwitterId | squote }},
          'app_secret' => ENV['OAUTH_TWITTER_SECRET']
        }
      ]

freedesktop:
  epochConfig: "v5" # keep in sync with omnibus/secrets
  pagesHosts:
    - docs.mesa3d.org
    - docs.pipewire.org
    - fprint.freedesktop.org
    - geoclue-test.freedesktop.org
    - libbsd-test.freedesktop.org
    - libcacard.freedesktop.org
    - libnice.freedesktop.org
    - libopenraw.freedesktop.org
    - mesa3d.org
    - mesa.freedesktop.org
    - mesa-test.freedesktop.org
    - monado.freedesktop.org
    - pages-test.freedesktop.org
    - panfrost.freedesktop.org
    - piglit.freedesktop.org
    - pipewire.org
    - poppler.freedesktop.org
    - specifications.freedesktop.org
    - wayland.freedesktop.org
    - www.mesa3d.org
    - www.pipewire.org
    - xrdesktop.freedesktop.org
    - zeitgeist.freedesktop.org
  ciHosts:
    - gitlab-ci-packet-old-dgc
    - gitlab-ci-packet-new-dfs
    - gitlab-ci-hetzner
  ingress:
    pagesServerSnippet: |
        set $new_uri "";
        if ($host = "mesa-test.freedesktop.org") {
          set $new_uri $mesa_uri;
        }
        if ($host = "mesa.freedesktop.org") {
          set $new_uri $mesa_uri;
        }
        if ($host = "mesa3d.org") {
          set $new_uri $mesa_uri;
        }
        if ($host = "www.mesa3d.org") {
          set $new_uri $mesa_uri;
        }
        if ($new_uri != "") {
          return 301 $new_uri;
        }

    serverSnippet: |
        set $target_host $host;
        set $redirect 0;

        # we can not use logical AND in operation:
        # use https://gist.github.com/jrom/1760790
        # as a workaround

        if ($host = "gitlab.freedesktop.org") {
          set $redirect "A";
        }
        
        if ($request_uri ~ ".*(git-upload-pack|artifacts)$") {
          set $redirect "${redirect}B";
        }
        
        # if ($remote_addr ~ "^51.83.77.207$") { # bentiss runner
        #   set $target_host gitlab-ci-packet-new-dfs.freedesktop.org;
        #   set $redirect "${redirect}C";
        # }

        # if ($remote_addr ~ "147.75.(6[4-9]|[78]\d|9[0-5]).\d*") { # packet runners
        #   set $target_host gitlab-ci-packet-new-dfs.freedesktop.org;
        #   set $redirect "${redirect}C";
        # }

        # if ($remote_addr ~ "139.178.(6[4-9]|[78]\d|9[0-5]).\d*") { # packet arm runners
        #   set $target_host gitlab-ci-packet-new-dfs.freedesktop.org;
        #   set $redirect "${redirect}C";
        # }

        if ($remote_addr ~ "147.75.64.(9|37)") { # packet runners (new dfs)
          set $target_host gitlab-ci-packet-new-dfs.freedesktop.org;
          set $redirect "${redirect}C";
        }

        if ($remote_addr ~ "139.178.70.6") { # packet arm runners (old dockergc)
          set $target_host gitlab-ci-packet-new-dfs.freedesktop.org;
          set $redirect "${redirect}C";
        }

        if ($remote_addr ~ "147.75.64.(39|157)") { # packet runners (old dockergc)
          set $target_host gitlab-ci-packet-old-dgc.freedesktop.org;
          set $redirect "${redirect}C";
        }

        if ($remote_addr ~ "139.178.94.2") { # packet arm runners (old dockergc)
          set $target_host gitlab-ci-packet-old-dgc.freedesktop.org;
          set $redirect "${redirect}C";
        }

        if ($remote_addr ~ "^95.21[67].*") { # hetzner runners
          set $target_host gitlab-ci-hetzner.freedesktop.org;
          set $redirect "${redirect}C";
        }

        if ($redirect = "ABC") {
          return 302 $scheme://$target_host$request_uri;
        }

global:
  edition: ce
  hosts:
    domain: freedesktop.org
    externalIP: 35.227.58.183
  omnibus:
    omnibusMemory: "50Gi"
  ingress:
    class: nginx
  grafana:
    enabled: true
  psql:
    database: gitlab_production
    username: gitlab
  appConfig:
    defaultCanCreateGroup: false
    omniauth:
      enabled: true
      allowSingleSignOn: true
      blockAutoCreatedUsers: false
      providers:
        - secret: gitlab-prod-freedesktop-omniauth-providers
          key: google
        - secret: gitlab-prod-freedesktop-omniauth-providers
          key: gitlab
        - secret: gitlab-prod-freedesktop-omniauth-providers
          key: github
        - secret: gitlab-prod-freedesktop-omniauth-providers
          key: twitter
    backups:
      enabled: true
      # Disabled in the chart due to freedesktop/helm-gitlab-omnibus@35a9e9326ba7
      proxy_download: false
      bucket: 'fdo-gitlab-data-backup'
      connection:
        secret: gitlab-prod-freedesktop-legacy-gcs-key
        key: connection
    uploads:
      enabled: true
      proxy_download: false
      bucket: 'fdo-gitlab-uploads'
      connection:
        secret: gitlab-prod-freedesktop-legacy-gcs-key
        key: connection
    artifacts:
      enabled: true
      proxy_download: false
      bucket: 'fdo-gitlab-artifacts'
      connection:
        secret: gitlab-prod-freedesktop-legacy-gcs-key
        key: connection
    lfs:
      enabled: true
      proxy_download: true
      bucket: 'fdo-gitlab-lfs'
      connection:
        secret: gitlab-prod-freedesktop-legacy-gcs-key
        key: connection
    incomingEmail:
      enabled: false
  smtp:
    enabled:  true
    address: 'gabe.freedesktop.org'
    port: 5878
    user_name: 'gitlab@gitlab.freedesktop.org'
    domain: 'gitlab.freedesktop.org'
    authentication: 'login'
    starttls_auto: true
    tls: false
    openssl_verify_mode: 'peer'
    password:
      secret: gitlab-prod-freedesktop-smtp-secret
  gitaly:
    enabled: false
    # provide a fake gitaly to have the config validated by helm
    external:
      - name: default                   # required
        hostname: gitlab-prod-gitlab    # required
  operator:
    enabled: false
  minio:
    enabled: false

gitlab:
  certmanager-issuer:
    email: daniel@fooishbar.org
  controller:
    ingressClass: nginx
  nginx-ingress:
    controller:
      extraArgs:
        enable-ssl-chain-completion: "false"
      config:
        http-snippet: |
            map $request_uri $mesa_uri {
              /application-issues.html https://docs.mesa3d.org/application-issues.html;
              /bugs.html https://docs.mesa3d.org/bugs.html;
              /codingstyle.html https://docs.mesa3d.org/codingstyle.html;
              /conform.html https://docs.mesa3d.org/conform.html;
              /contents.html https://docs.mesa3d.org/contents.html;
              /debugging.html https://docs.mesa3d.org/debugging.html;
              /developers.html https://docs.mesa3d.org/developers.html;
              /devinfo.html https://docs.mesa3d.org/devinfo.html;
              /dispatch.html https://docs.mesa3d.org/dispatch.html;
              /download.html https://docs.mesa3d.org/download.html;
              /egl.html https://docs.mesa3d.org/egl.html;
              /envvars.html https://docs.mesa3d.org/envvars.html;
              /extensions.html https://docs.mesa3d.org/extensions.html;
              /faq.html https://docs.mesa3d.org/faq.html;
              /helpwanted.html https://docs.mesa3d.org/helpwanted.html;
              /install.html https://docs.mesa3d.org/install.html;
              /intro.html https://docs.mesa3d.org/intro.html;
              /license.html https://docs.mesa3d.org/license.html;
              /lists.html https://docs.mesa3d.org/lists.html;
              /llvmpipe.html https://docs.mesa3d.org/llvmpipe.html;
              /meson.html https://docs.mesa3d.org/meson.html;
              /opengles.html https://docs.mesa3d.org/opengles.html;
              /osmesa.html https://docs.mesa3d.org/osmesa.html;
              /perf.html https://docs.mesa3d.org/perf.html;
              /postprocess.html https://docs.mesa3d.org/postprocess.html;
              /precompiled.html https://docs.mesa3d.org/precompiled.html;
              /release-calendar.html https://docs.mesa3d.org/release-calendar.html;
              /releasing.html https://docs.mesa3d.org/releasing.html;
              /relnotes/17.1.4.html https://docs.mesa3d.org/relnotes/17.1.4.html;
              /relnotes/6.5.2.html https://docs.mesa3d.org/relnotes/6.5.2.html;
              /relnotes/17.0.1.html https://docs.mesa3d.org/relnotes/17.0.1.html;
              /relnotes/17.0.3.html https://docs.mesa3d.org/relnotes/17.0.3.html;
              /relnotes/18.2.7.html https://docs.mesa3d.org/relnotes/18.2.7.html;
              /relnotes/7.7.html https://docs.mesa3d.org/relnotes/7.7.html;
              /relnotes/17.1.6.html https://docs.mesa3d.org/relnotes/17.1.6.html;
              /relnotes/18.1.9.html https://docs.mesa3d.org/relnotes/18.1.9.html;
              /relnotes/17.3.3.html https://docs.mesa3d.org/relnotes/17.3.3.html;
              /relnotes/7.9.html https://docs.mesa3d.org/relnotes/7.9.html;
              /relnotes/10.5.7.html https://docs.mesa3d.org/relnotes/10.5.7.html;
              /relnotes/20.0.7.html https://docs.mesa3d.org/relnotes/20.0.7.html;
              /relnotes/6.5.3.html https://docs.mesa3d.org/relnotes/6.5.3.html;
              /relnotes/7.10.html https://docs.mesa3d.org/relnotes/7.10.html;
              /relnotes/10.4.1.html https://docs.mesa3d.org/relnotes/10.4.1.html;
              /relnotes/17.3.4.html https://docs.mesa3d.org/relnotes/17.3.4.html;
              /relnotes/17.1.5.html https://docs.mesa3d.org/relnotes/17.1.5.html;
              /relnotes/10.6.1.html https://docs.mesa3d.org/relnotes/10.6.1.html;
              /relnotes/9.2.2.html https://docs.mesa3d.org/relnotes/9.2.2.html;
              /relnotes/8.0.1.html https://docs.mesa3d.org/relnotes/8.0.1.html;
              /relnotes/20.0.6.html https://docs.mesa3d.org/relnotes/20.0.6.html;
              /relnotes/10.6.2.html https://docs.mesa3d.org/relnotes/10.6.2.html;
              /relnotes/10.1.html https://docs.mesa3d.org/relnotes/10.1.html;
              /relnotes/19.1.8.html https://docs.mesa3d.org/relnotes/19.1.8.html;
              /relnotes/18.0.5.html https://docs.mesa3d.org/relnotes/18.0.5.html;
              /relnotes/20.0.1.html https://docs.mesa3d.org/relnotes/20.0.1.html;
              /relnotes/17.1.3.html https://docs.mesa3d.org/relnotes/17.1.3.html;
              /relnotes/10.5.2.html https://docs.mesa3d.org/relnotes/10.5.2.html;
              /relnotes/10.0.html https://docs.mesa3d.org/relnotes/10.0.html;
              /relnotes/8.0.html https://docs.mesa3d.org/relnotes/8.0.html;
              /relnotes/19.2.7.html https://docs.mesa3d.org/relnotes/19.2.7.html;
              /relnotes/18.2.1.html https://docs.mesa3d.org/relnotes/18.2.1.html;
              /relnotes/7.8.html https://docs.mesa3d.org/relnotes/7.8.html;
              /relnotes/9.2.1.html https://docs.mesa3d.org/relnotes/9.2.1.html;
              /relnotes/10.6.6.html https://docs.mesa3d.org/relnotes/10.6.6.html;
              /relnotes/9.0.html https://docs.mesa3d.org/relnotes/9.0.html;
              /relnotes/7.4.html https://docs.mesa3d.org/relnotes/7.4.html;
              /relnotes/19.1.1.html https://docs.mesa3d.org/relnotes/19.1.1.html;
              /relnotes/12.0.6.html https://docs.mesa3d.org/relnotes/12.0.6.html;
              /relnotes/9.2.4.html https://docs.mesa3d.org/relnotes/9.2.4.html;
              /relnotes/7.0.1.html https://docs.mesa3d.org/relnotes/7.0.1.html;
              /relnotes/17.1.10.html https://docs.mesa3d.org/relnotes/17.1.10.html;
              /relnotes/7.5.1.html https://docs.mesa3d.org/relnotes/7.5.1.html;
              /relnotes/17.0.7.html https://docs.mesa3d.org/relnotes/17.0.7.html;
              /relnotes/7.4.1.html https://docs.mesa3d.org/relnotes/7.4.1.html;
              /relnotes/17.2.7.html https://docs.mesa3d.org/relnotes/17.2.7.html;
              /relnotes/10.4.2.html https://docs.mesa3d.org/relnotes/10.4.2.html;
              /relnotes/19.0.4.html https://docs.mesa3d.org/relnotes/19.0.4.html;
              /relnotes/7.9.2.html https://docs.mesa3d.org/relnotes/7.9.2.html;
              /relnotes/17.0.0.html https://docs.mesa3d.org/relnotes/17.0.0.html;
              /relnotes/19.2.5.html https://docs.mesa3d.org/relnotes/19.2.5.html;
              /relnotes/18.0.1.html https://docs.mesa3d.org/relnotes/18.0.1.html;
              /relnotes/10.2.2.html https://docs.mesa3d.org/relnotes/10.2.2.html;
              /relnotes/17.2.3.html https://docs.mesa3d.org/relnotes/17.2.3.html;
              /relnotes/6.5.html https://docs.mesa3d.org/relnotes/6.5.html;
              /relnotes/17.3.0.html https://docs.mesa3d.org/relnotes/17.3.0.html;
              /relnotes/6.4.1.html https://docs.mesa3d.org/relnotes/6.4.1.html;
              /relnotes/19.2.1.html https://docs.mesa3d.org/relnotes/19.2.1.html;
              /relnotes/10.2.3.html https://docs.mesa3d.org/relnotes/10.2.3.html;
              /relnotes/19.1.5.html https://docs.mesa3d.org/relnotes/19.1.5.html;
              /relnotes/13.0.1.html https://docs.mesa3d.org/relnotes/13.0.1.html;
              /relnotes/18.1.2.html https://docs.mesa3d.org/relnotes/18.1.2.html;
              /relnotes/10.3.5.html https://docs.mesa3d.org/relnotes/10.3.5.html;
              /relnotes/10.2.9.html https://docs.mesa3d.org/relnotes/10.2.9.html;
              /relnotes/11.0.5.html https://docs.mesa3d.org/relnotes/11.0.5.html;
              /relnotes/10.1.2.html https://docs.mesa3d.org/relnotes/10.1.2.html;
              /relnotes/7.2.html https://docs.mesa3d.org/relnotes/7.2.html;
              /relnotes/20.1.0.html https://docs.mesa3d.org/relnotes/20.1.0.html;
              /relnotes/10.0.1.html https://docs.mesa3d.org/relnotes/10.0.1.html;
              /relnotes/17.2.0.html https://docs.mesa3d.org/relnotes/17.2.0.html;
              /relnotes/10.5.1.html https://docs.mesa3d.org/relnotes/10.5.1.html;
              /relnotes/17.1.8.html https://docs.mesa3d.org/relnotes/17.1.8.html;
              /relnotes/19.3.0.html https://docs.mesa3d.org/relnotes/19.3.0.html;
              /relnotes/18.3.5.html https://docs.mesa3d.org/relnotes/18.3.5.html;
              /relnotes/18.3.1.html https://docs.mesa3d.org/relnotes/18.3.1.html;
              /relnotes/10.4.html https://docs.mesa3d.org/relnotes/10.4.html;
              /relnotes/20.0.2.html https://docs.mesa3d.org/relnotes/20.0.2.html;
              /relnotes/7.11.html https://docs.mesa3d.org/relnotes/7.11.html;
              /relnotes/10.2.html https://docs.mesa3d.org/relnotes/10.2.html;
              /relnotes/7.10.3.html https://docs.mesa3d.org/relnotes/7.10.3.html;
              /relnotes/18.2.2.html https://docs.mesa3d.org/relnotes/18.2.2.html;
              /relnotes/10.5.8.html https://docs.mesa3d.org/relnotes/10.5.8.html;
              /relnotes/9.1.1.html https://docs.mesa3d.org/relnotes/9.1.1.html;
              /relnotes/18.2.0.html https://docs.mesa3d.org/relnotes/18.2.0.html;
              /relnotes/12.0.5.html https://docs.mesa3d.org/relnotes/12.0.5.html;
              /relnotes/8.0.5.html https://docs.mesa3d.org/relnotes/8.0.5.html;
              /relnotes/10.6.0.html https://docs.mesa3d.org/relnotes/10.6.0.html;
              /relnotes/10.4.4.html https://docs.mesa3d.org/relnotes/10.4.4.html;
              /relnotes/10.1.1.html https://docs.mesa3d.org/relnotes/10.1.1.html;
              /relnotes/7.10.2.html https://docs.mesa3d.org/relnotes/7.10.2.html;
              /relnotes/10.2.6.html https://docs.mesa3d.org/relnotes/10.2.6.html;
              /relnotes/10.6.9.html https://docs.mesa3d.org/relnotes/10.6.9.html;
              /relnotes/13.0.6.html https://docs.mesa3d.org/relnotes/13.0.6.html;
              /relnotes/10.3.2.html https://docs.mesa3d.org/relnotes/10.3.2.html;
              /relnotes/19.2.0.html https://docs.mesa3d.org/relnotes/19.2.0.html;
              /relnotes/19.3.3.html https://docs.mesa3d.org/relnotes/19.3.3.html;
              /relnotes/17.1.1.html https://docs.mesa3d.org/relnotes/17.1.1.html;
              /relnotes/10.2.8.html https://docs.mesa3d.org/relnotes/10.2.8.html;
              /relnotes/17.0.5.html https://docs.mesa3d.org/relnotes/17.0.5.html;
              /relnotes/11.0.7.html https://docs.mesa3d.org/relnotes/11.0.7.html;
              /relnotes/11.1.0.html https://docs.mesa3d.org/relnotes/11.1.0.html;
              /relnotes/10.3.1.html https://docs.mesa3d.org/relnotes/10.3.1.html;
              /relnotes/20.0.5.html https://docs.mesa3d.org/relnotes/20.0.5.html;
              /relnotes/7.0.2.html https://docs.mesa3d.org/relnotes/7.0.2.html;
              /relnotes/10.4.3.html https://docs.mesa3d.org/relnotes/10.4.3.html;
              /relnotes/11.1.1.html https://docs.mesa3d.org/relnotes/11.1.1.html;
              /relnotes/6.4.html https://docs.mesa3d.org/relnotes/6.4.html;
              /relnotes/11.2.0.html https://docs.mesa3d.org/relnotes/11.2.0.html;
              /relnotes/17.1.2.html https://docs.mesa3d.org/relnotes/17.1.2.html;
              /relnotes/9.2.5.html https://docs.mesa3d.org/relnotes/9.2.5.html;
              /relnotes/10.1.6.html https://docs.mesa3d.org/relnotes/10.1.6.html;
              /relnotes/18.1.7.html https://docs.mesa3d.org/relnotes/18.1.7.html;
              /relnotes/10.1.4.html https://docs.mesa3d.org/relnotes/10.1.4.html;
              /relnotes/6.4.2.html https://docs.mesa3d.org/relnotes/6.4.2.html;
              /relnotes/10.3.4.html https://docs.mesa3d.org/relnotes/10.3.4.html;
              /relnotes/8.0.4.html https://docs.mesa3d.org/relnotes/8.0.4.html;
              /relnotes/10.0.3.html https://docs.mesa3d.org/relnotes/10.0.3.html;
              /relnotes/7.9.1.html https://docs.mesa3d.org/relnotes/7.9.1.html;
              /relnotes/9.1.3.html https://docs.mesa3d.org/relnotes/9.1.3.html;
              /relnotes/19.1.3.html https://docs.mesa3d.org/relnotes/19.1.3.html;
              /relnotes/18.3.2.html https://docs.mesa3d.org/relnotes/18.3.2.html;
              /relnotes/19.1.2.html https://docs.mesa3d.org/relnotes/19.1.2.html;
              /relnotes/7.6.html https://docs.mesa3d.org/relnotes/7.6.html;
              /relnotes/12.0.4.html https://docs.mesa3d.org/relnotes/12.0.4.html;
              /relnotes/7.1.html https://docs.mesa3d.org/relnotes/7.1.html;
              /relnotes/10.3.3.html https://docs.mesa3d.org/relnotes/10.3.3.html;
              /relnotes/10.3.html https://docs.mesa3d.org/relnotes/10.3.html;
              /relnotes/9.1.2.html https://docs.mesa3d.org/relnotes/9.1.2.html;
              /relnotes/9.1.5.html https://docs.mesa3d.org/relnotes/9.1.5.html;
              /relnotes/18.2.6.html https://docs.mesa3d.org/relnotes/18.2.6.html;
              /relnotes/7.11.2.html https://docs.mesa3d.org/relnotes/7.11.2.html;
              /relnotes/17.2.5.html https://docs.mesa3d.org/relnotes/17.2.5.html;
              /relnotes/11.0.9.html https://docs.mesa3d.org/relnotes/11.0.9.html;
              /relnotes/19.0.8.html https://docs.mesa3d.org/relnotes/19.0.8.html;
              /relnotes/9.0.2.html https://docs.mesa3d.org/relnotes/9.0.2.html;
              /relnotes/18.1.4.html https://docs.mesa3d.org/relnotes/18.1.4.html;
              /relnotes/12.0.3.html https://docs.mesa3d.org/relnotes/12.0.3.html;
              /relnotes/19.1.4.html https://docs.mesa3d.org/relnotes/19.1.4.html;
              /relnotes/17.0.6.html https://docs.mesa3d.org/relnotes/17.0.6.html;
              /relnotes/17.2.2.html https://docs.mesa3d.org/relnotes/17.2.2.html;
              /relnotes/17.3.6.html https://docs.mesa3d.org/relnotes/17.3.6.html;
              /relnotes/9.2.3.html https://docs.mesa3d.org/relnotes/9.2.3.html;
              /relnotes/10.6.7.html https://docs.mesa3d.org/relnotes/10.6.7.html;
              /relnotes/10.6.8.html https://docs.mesa3d.org/relnotes/10.6.8.html;
              /relnotes/19.3.1.html https://docs.mesa3d.org/relnotes/19.3.1.html;
              /relnotes/7.0.4.html https://docs.mesa3d.org/relnotes/7.0.4.html;
              /relnotes/9.1.4.html https://docs.mesa3d.org/relnotes/9.1.4.html;
              /relnotes/17.2.6.html https://docs.mesa3d.org/relnotes/17.2.6.html;
              /relnotes/7.0.3.html https://docs.mesa3d.org/relnotes/7.0.3.html;
              /relnotes/11.0.1.html https://docs.mesa3d.org/relnotes/11.0.1.html;
              /relnotes/10.3.6.html https://docs.mesa3d.org/relnotes/10.3.6.html;
              /relnotes/19.0.0.html https://docs.mesa3d.org/relnotes/19.0.0.html;
              /relnotes/9.1.html https://docs.mesa3d.org/relnotes/9.1.html;
              /relnotes/18.1.3.html https://docs.mesa3d.org/relnotes/18.1.3.html;
              /relnotes/20.0.8.html https://docs.mesa3d.org/relnotes/20.0.8.html;
              /relnotes/10.0.5.html https://docs.mesa3d.org/relnotes/10.0.5.html;
              /relnotes/20.1.1.html https://docs.mesa3d.org/relnotes/20.1.1.html;
              /relnotes/17.0.4.html https://docs.mesa3d.org/relnotes/17.0.4.html;
              /relnotes/11.2.2.html https://docs.mesa3d.org/relnotes/11.2.2.html;
              /relnotes/18.2.5.html https://docs.mesa3d.org/relnotes/18.2.5.html;
              /relnotes/18.3.4.html https://docs.mesa3d.org/relnotes/18.3.4.html;
              /relnotes/19.0.6.html https://docs.mesa3d.org/relnotes/19.0.6.html;
              /relnotes/9.2.html https://docs.mesa3d.org/relnotes/9.2.html;
              /relnotes/18.0.4.html https://docs.mesa3d.org/relnotes/18.0.4.html;
              /relnotes/17.0.2.html https://docs.mesa3d.org/relnotes/17.0.2.html;
              /relnotes/10.6.5.html https://docs.mesa3d.org/relnotes/10.6.5.html;
              /relnotes/11.1.3.html https://docs.mesa3d.org/relnotes/11.1.3.html;
              /relnotes/9.0.1.html https://docs.mesa3d.org/relnotes/9.0.1.html;
              /relnotes/9.1.7.html https://docs.mesa3d.org/relnotes/9.1.7.html;
              /relnotes/7.8.3.html https://docs.mesa3d.org/relnotes/7.8.3.html;
              /relnotes/10.5.6.html https://docs.mesa3d.org/relnotes/10.5.6.html;
              /relnotes/12.0.2.html https://docs.mesa3d.org/relnotes/12.0.2.html;
              /relnotes/17.2.4.html https://docs.mesa3d.org/relnotes/17.2.4.html;
              /relnotes/17.2.1.html https://docs.mesa3d.org/relnotes/17.2.1.html;
              /relnotes/8.0.3.html https://docs.mesa3d.org/relnotes/8.0.3.html;
              /relnotes/7.10.1.html https://docs.mesa3d.org/relnotes/7.10.1.html;
              /relnotes/7.3.html https://docs.mesa3d.org/relnotes/7.3.html;
              /relnotes/11.0.4.html https://docs.mesa3d.org/relnotes/11.0.4.html;
              /relnotes/19.2.4.html https://docs.mesa3d.org/relnotes/19.2.4.html;
              /relnotes/7.4.2.html https://docs.mesa3d.org/relnotes/7.4.2.html;
              /relnotes/17.3.8.html https://docs.mesa3d.org/relnotes/17.3.8.html;
              /relnotes/7.4.4.html https://docs.mesa3d.org/relnotes/7.4.4.html;
              /relnotes/19.3.4.html https://docs.mesa3d.org/relnotes/19.3.4.html;
              /relnotes/11.0.0.html https://docs.mesa3d.org/relnotes/11.0.0.html;
              /relnotes/10.2.1.html https://docs.mesa3d.org/relnotes/10.2.1.html;
              /relnotes/17.3.2.html https://docs.mesa3d.org/relnotes/17.3.2.html;
              /relnotes/17.3.1.html https://docs.mesa3d.org/relnotes/17.3.1.html;
              /relnotes/10.5.4.html https://docs.mesa3d.org/relnotes/10.5.4.html;
              /relnotes/7.4.3.html https://docs.mesa3d.org/relnotes/7.4.3.html;
              /relnotes/17.1.7.html https://docs.mesa3d.org/relnotes/17.1.7.html;
              /relnotes/17.3.7.html https://docs.mesa3d.org/relnotes/17.3.7.html;
              /relnotes/7.5.2.html https://docs.mesa3d.org/relnotes/7.5.2.html;
              /relnotes/17.2.8.html https://docs.mesa3d.org/relnotes/17.2.8.html;
              /relnotes/10.1.5.html https://docs.mesa3d.org/relnotes/10.1.5.html;
              /relnotes/10.6.4.html https://docs.mesa3d.org/relnotes/10.6.4.html;
              /relnotes/11.0.2.html https://docs.mesa3d.org/relnotes/11.0.2.html;
              /relnotes/10.5.9.html https://docs.mesa3d.org/relnotes/10.5.9.html;
              /relnotes/18.1.0.html https://docs.mesa3d.org/relnotes/18.1.0.html;
              /relnotes/19.1.0.html https://docs.mesa3d.org/relnotes/19.1.0.html;
              /relnotes/19.2.2.html https://docs.mesa3d.org/relnotes/19.2.2.html;
              /relnotes/19.0.3.html https://docs.mesa3d.org/relnotes/19.0.3.html;
              /relnotes/10.5.3.html https://docs.mesa3d.org/relnotes/10.5.3.html;
              /relnotes/17.3.5.html https://docs.mesa3d.org/relnotes/17.3.5.html;
              /relnotes/11.2.1.html https://docs.mesa3d.org/relnotes/11.2.1.html;
              /relnotes/7.6.1.html https://docs.mesa3d.org/relnotes/7.6.1.html;
              /relnotes/10.4.6.html https://docs.mesa3d.org/relnotes/10.4.6.html;
              /relnotes/10.2.7.html https://docs.mesa3d.org/relnotes/10.2.7.html;
              /relnotes/18.3.6.html https://docs.mesa3d.org/relnotes/18.3.6.html;
              /relnotes/18.2.8.html https://docs.mesa3d.org/relnotes/18.2.8.html;
              /relnotes/13.0.3.html https://docs.mesa3d.org/relnotes/13.0.3.html;
              /relnotes/10.0.4.html https://docs.mesa3d.org/relnotes/10.0.4.html;
              /relnotes/7.8.2.html https://docs.mesa3d.org/relnotes/7.8.2.html;
              /relnotes/18.2.3.html https://docs.mesa3d.org/relnotes/18.2.3.html;
              /relnotes/20.0.0.html https://docs.mesa3d.org/relnotes/20.0.0.html;
              /relnotes/11.0.3.html https://docs.mesa3d.org/relnotes/11.0.3.html;
              /relnotes/10.5.0.html https://docs.mesa3d.org/relnotes/10.5.0.html;
              /relnotes/19.1.7.html https://docs.mesa3d.org/relnotes/19.1.7.html;
              /relnotes/10.1.3.html https://docs.mesa3d.org/relnotes/10.1.3.html;
              /relnotes/19.2.3.html https://docs.mesa3d.org/relnotes/19.2.3.html;
              /relnotes/7.11.1.html https://docs.mesa3d.org/relnotes/7.11.1.html;
              /relnotes/18.1.8.html https://docs.mesa3d.org/relnotes/18.1.8.html;
              /relnotes/10.4.7.html https://docs.mesa3d.org/relnotes/10.4.7.html;
              /relnotes/17.1.0.html https://docs.mesa3d.org/relnotes/17.1.0.html;
              /relnotes/11.0.6.html https://docs.mesa3d.org/relnotes/11.0.6.html;
              /relnotes/8.0.2.html https://docs.mesa3d.org/relnotes/8.0.2.html;
              /relnotes/17.1.9.html https://docs.mesa3d.org/relnotes/17.1.9.html;
              /relnotes/13.0.0.html https://docs.mesa3d.org/relnotes/13.0.0.html;
              /relnotes/18.2.4.html https://docs.mesa3d.org/relnotes/18.2.4.html;
              /relnotes/6.5.1.html https://docs.mesa3d.org/relnotes/6.5.1.html;
              /relnotes/19.2.6.html https://docs.mesa3d.org/relnotes/19.2.6.html;
              /relnotes/13.0.4.html https://docs.mesa3d.org/relnotes/13.0.4.html;
              /relnotes/12.0.0.html https://docs.mesa3d.org/relnotes/12.0.0.html;
              /relnotes/10.6.3.html https://docs.mesa3d.org/relnotes/10.6.3.html;
              /relnotes/18.3.3.html https://docs.mesa3d.org/relnotes/18.3.3.html;
              /relnotes/9.0.3.html https://docs.mesa3d.org/relnotes/9.0.3.html;
              /relnotes/18.3.0.html https://docs.mesa3d.org/relnotes/18.3.0.html;
              /relnotes/7.7.1.html https://docs.mesa3d.org/relnotes/7.7.1.html;
              /relnotes/10.2.5.html https://docs.mesa3d.org/relnotes/10.2.5.html;
              /relnotes/11.1.4.html https://docs.mesa3d.org/relnotes/11.1.4.html;
              /relnotes/20.0.3.html https://docs.mesa3d.org/relnotes/20.0.3.html;
              /relnotes/19.1.6.html https://docs.mesa3d.org/relnotes/19.1.6.html;
              /relnotes/7.8.1.html https://docs.mesa3d.org/relnotes/7.8.1.html;
              /relnotes/20.0.4.html https://docs.mesa3d.org/relnotes/20.0.4.html;
              /relnotes/18.0.0.html https://docs.mesa3d.org/relnotes/18.0.0.html;
              /relnotes/19.0.7.html https://docs.mesa3d.org/relnotes/19.0.7.html;
              /relnotes/10.4.5.html https://docs.mesa3d.org/relnotes/10.4.5.html;
              /relnotes/19.0.1.html https://docs.mesa3d.org/relnotes/19.0.1.html;
              /relnotes/19.3.2.html https://docs.mesa3d.org/relnotes/19.3.2.html;
              /relnotes/9.1.6.html https://docs.mesa3d.org/relnotes/9.1.6.html;
              /relnotes/17.3.9.html https://docs.mesa3d.org/relnotes/17.3.9.html;
              /relnotes/7.5.html https://docs.mesa3d.org/relnotes/7.5.html;
              /relnotes/18.1.1.html https://docs.mesa3d.org/relnotes/18.1.1.html;
              /relnotes/10.5.5.html https://docs.mesa3d.org/relnotes/10.5.5.html;
              /relnotes/19.0.2.html https://docs.mesa3d.org/relnotes/19.0.2.html;
              /relnotes/7.0.html https://docs.mesa3d.org/relnotes/7.0.html;
              /relnotes/18.1.6.html https://docs.mesa3d.org/relnotes/18.1.6.html;
              /relnotes/13.0.5.html https://docs.mesa3d.org/relnotes/13.0.5.html;
              /relnotes/10.0.2.html https://docs.mesa3d.org/relnotes/10.0.2.html;
              /relnotes/11.0.8.html https://docs.mesa3d.org/relnotes/11.0.8.html;
              /relnotes/19.2.8.html https://docs.mesa3d.org/relnotes/19.2.8.html;
              /relnotes/10.3.7.html https://docs.mesa3d.org/relnotes/10.3.7.html;
              /relnotes/18.0.3.html https://docs.mesa3d.org/relnotes/18.0.3.html;
              /relnotes/13.0.2.html https://docs.mesa3d.org/relnotes/13.0.2.html;
              /relnotes/12.0.1.html https://docs.mesa3d.org/relnotes/12.0.1.html;
              /relnotes/10.2.4.html https://docs.mesa3d.org/relnotes/10.2.4.html;
              /relnotes/19.3.5.html https://docs.mesa3d.org/relnotes/19.3.5.html;
              /relnotes/18.1.5.html https://docs.mesa3d.org/relnotes/18.1.5.html;
              /relnotes/11.1.2.html https://docs.mesa3d.org/relnotes/11.1.2.html;
              /relnotes/19.0.5.html https://docs.mesa3d.org/relnotes/19.0.5.html;
              /relnotes/18.0.2.html https://docs.mesa3d.org/relnotes/18.0.2.html;
              /relnotes.html https://docs.mesa3d.org/relnotes.html;
              /repository.html https://docs.mesa3d.org/repository.html;
              /shading.html https://docs.mesa3d.org/shading.html;
              /sourcedocs.html https://docs.mesa3d.org/sourcedocs.html;
              /sourcetree.html https://docs.mesa3d.org/sourcetree.html;
              /submittingpatches.html https://docs.mesa3d.org/submittingpatches.html;
              /systems.html https://docs.mesa3d.org/systems.html;
              /thanks.html https://docs.mesa3d.org/thanks.html;
              /utilities.html https://docs.mesa3d.org/utilities.html;
              /versions.html https://docs.mesa3d.org/versions.html;
              /viewperf.html https://docs.mesa3d.org/viewperf.html;
              /vmware-guest.html https://docs.mesa3d.org/vmware-guest.html;
              /webmaster.html https://www.mesa3d.org/website/;
              /xlibdriver.html https://docs.mesa3d.org/xlibdriver.html;
              ~/archive(.*) https://archive.mesa3d.org/$1;
              default "";
            }

  gitlab:
    geo-logcursor:
      enabled: false
    gitlab-grafana:
      ingress:
        annotations:
          nginx.ingress.kubernetes.io/permanent-redirect: https://grafana.freedesktop.org
    gitlab-shell:
      enabled: false
    migrations:
      enabled: false
    sidekiq:
      enabled: false
    task-runner:
      enabled: false
    webservice:
      enabled: true
      registry:
        tokenIssuer: 'omnibus-gitlab-issuer'
      # we provide our own ingress with special rules to get more
      # stats from prometheus
      ingress:
        enabled: false
        proxyBodySize: '0'
      rack_attack:
        git_basic_auth:
          enabled: false
          ip_whitelist: 
            - '127.0.0.1'
          maxretry: 10  # Limit the number of Git HTTP authentication attempts per IP$
          findtime: 360 # Reset the auth attempt counter per IP after 60 seconds$
          bantime: 3600 # Ban an IP for one hour (3600s) after too many auth attempts$
  gitlab-runner:
    install: false
  grafana:
    resources:
      requests:
        memory: "2Gi"
        cpu: "1"
    persistence:
      enabled: true
      storageClassName: standard
    grafana.ini:
      auth.gitlab:
        enabled: true
        allow_sign_up: true
        scopes: read_user
        auth_url: https://gitlab.freedesktop.org/oauth/authorize
        token_url: https://gitlab.freedesktop.org/oauth/token
        api_url: https://gitlab.freedesktop.org/api/v4
      server:
        root_url: https://grafana.freedesktop.org/
  prometheus:
    nodeExporter:
      enabled: true
    server:
      resources:
        requests:
          memory: "4Gi"
          cpu: "2"
  postgresql:
    resources:
      requests:
        memory: "16Gi"
        cpu: "2"
    persistence:
      storageClass: gitlab-prod-gitlab-fast
      size: "30Gi"
    postgresqlDatabase: gitlab_production
    postgresqlExtendedConf:
      max_connections: 300
  redis:
    master:
      resources:
        requests:
          memory: "8Gi"
          cpu: "2"
    persistence:
      existingClaim: gitlab-prod-gitlab-redis-storage
  registry:
    enabled: true
    maintenance:
      readOnly:
        enabled: false
    ingress:
      enabled: true
    storage:
      secret: gitlab-prod-freedesktop-registry-gcs-config
      key: 'config'
      extraKey: 'keyfile'
    tokenIssuer: 'omnibus-gitlab-issuer'
    log:
      level: info
  upgradeCheck:
    enabled: false
